#!/bin/sh

VERSION=0.0.1
name=registry.gitlab.com/chubby-chocobo/example-spring
version=${VERSION}
username="gitlab-ci-token"

while [ $# -gt 0 ]; do
  VALUE="${1#*=}"
  case "$1" in
    --token=*)
      token=${VALUE}
      ;;
    --username=*)
      username=${VALUE}
      ;;
    --version=*)
      version=${VALUE}
      ;;
    --help)
      echo "Please add argument --token=YOUR_TOKEN"
      ;;
    *)
      printf "* Error: Invalid token. Please use --help for manual*"
      exit 1
  esac
  shift
done

# Update Docker Registry

docker login -u "${username}" -p "${token}" registry.gitlab.com

docker push "${name}:${version}"
