#!/bin/sh

VERSION=0.0.1
name=registry.gitlab.com/chubby-chocobo/example-spring
version=${VERSION}

while [ $# -gt 0 ]; do
  VALUE="${1#*=}"
  case "$1" in
    --version=*)
      version=${VALUE}
      ;;
    --help)
      echo "Please add version --version=YOUR_TAG_VERSION"
      ;;
    *)
      printf "* Error: Invalid token. Please use --help for manual*"
      exit 1
  esac
  shift
done

echo "Build image..."
# Build api-payments image
docker build -t "${name}:${version}" .
echo "Build end"
