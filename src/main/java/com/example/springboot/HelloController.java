package com.example.springboot;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

@RestController
public class HelloController {

	@RequestMapping("/")
	public String index() {
		return "Greetings from Spring Boot!";
	}

	@RequestMapping("/api/example-spring/test1")
	public String test1() {
		return "test1:" + System.getProperty("PASSWORD");
	}

	@RequestMapping("/api/example-spring/test2")
	public String test2() {
		return "test2:" + System.getProperty("PASSWORD");
	}

}
